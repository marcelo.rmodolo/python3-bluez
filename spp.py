import dbus, time
 
import dbus
import dbus.service
import dbus.mainloop.glib
try:
    from gi.repository import GObject
except ImportError:
    import gobject as GObject


bus = dbus.SystemBus()

class SPP:

    def __init__(self):
        self.profile = None
        manager = dbus.Interface(bus.get_object('org.bluez',
                                                '/org/bluez'),
                                 'org.bluez.ProfileManager1')

        self.mainloop = GObject.MainLoop()
        adapter_props = dbus.Interface(bus.get_object('org.bluez', '/org/bluez/hci0'),
                                       'org.freedesktop.DBus.Properties')

        adapter_props.Set('org.bluez.Adapter1', 'Powered', dbus.Boolean(1))
        profile_path = '/foo/bar/profile'
        server_uuid = '00001101-0000-1000-8000-00805f9b34fb'
        opts = {
            'AutoConnect': True,
            # 'Role': 'client',
            'Channel': dbus.UInt16(22),
            'Name': 'SerialPort'
        }

        print('Starting Serial Port Profile...')

        manager.RegisterProfile(profile_path, server_uuid, opts)

    def start(self):
        self.mainloop.run()


service_record = """
<?xml version="1.0" encoding="UTF-8" ?>
<record>
  <attribute id="0x0001">
    <sequence>
      <uuid value="0x1101"/>
    </sequence>
  </attribute>
  <attribute id="0x0004">
    <sequence>
      <sequence>
        <uuid value="0x0100"/>
      </sequence>
      <sequence>
        <uuid value="0x0003"/>
        <uint8 value="1" name="channel"/>
      </sequence>
    </sequence>
  </attribute>
  <attribute id="0x0100">
    <text value="Serial Port" name="name"/>
  </attribute>
</record>
"""
 
# bus = dbus.SystemBus()
# manager = dbus.Interface(bus.get_object("org.bluez", "/org/bluez"),
#                         "org.bluez.ProfileManager1")
# i = manager.RegisterProfile("/bluez",
#                         "00001101-0000-1000-8000-00805f9b34fb",
#                         {"AutoConnect":True, "ServiceRecord":service_record})
# 
# print(manager)
# print(i)

spp = SPP()
spp.start()

# while True:
#    time.sleep(1)
