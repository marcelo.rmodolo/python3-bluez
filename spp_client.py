#!/usr/bin/env python3

from gi.repository import GObject
import spp_server


class Sensors(object):
    def __init__(self):
        self._cores = {
                0: "/sys/devices/platform/coretemp.0/hwmon/hwmon3/temp2_input",
                1: "/sys/devices/platform/coretemp.0/hwmon/hwmon3/temp3_input"}

    def cpu(self, core):
        with open(self._cores[core], 'r') as core_file:
            value = core_file.readline()
            return float(value)/1000.0


def my_read_cb(value):
    print('my callback: {}'.format(value))


def my_write_cb(my_server):
    sensors = Sensors()
    msg = 'Core0: {}, Core1: {}\n'.format(sensors.cpu(0), sensors.cpu(1))
    if my_server.fd_available():
        my_server.write_spp(msg)
        print('Sending: {}'.format(msg))
    return True


if __name__ == '__main__':
    my_spp_server = spp_server.SPP(my_read_cb)
    GObject.timeout_add(1000, my_write_cb, my_spp_server)
    my_spp_server.start()
