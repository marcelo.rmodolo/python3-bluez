# python3-bluez

Communication between an Ubuntu desktop and an Android phone

[Setting Up Bluetooth Serial Port Profile on Raspberry Pi using D-Bus API](https://scribles.net/setting-up-bluetooth-serial-port-profile-on-raspberry-pi-using-d-bus-api/)

I did the step by step I didn't succeed

```
$ sudo apt install minicom -y
$ python spp.py
$ sudo rfcomm watch hci0
$ minicom -b 9600 -o -D /dev/rfcomm0
```

[Using Python to interface to Bluez](https://ukbaz.github.io/howto/AppInventor.html)

It worked with small changes!

[dbus-python: Python bindings for D-Bus](https://dbus.freedesktop.org/doc/dbus-python/index.html#dbus-python-python-bindings-for-d-bus)

Problems and alternatives
dbus-python might not be the best D-Bus binding for you to use. dbus-python does not follow the principle of “In the face of ambiguity, refuse the temptation to guess”, and can’t be changed to not do so without seriously breaking compatibility.

In addition, it uses libdbus (which has known problems with multi-threaded use) and attempts to be main-loop-agnostic (which means you have to select a suitable main loop for your application).

Alternative ways to get your Python code onto D-Bus include:

- GDBus, part of the GIO module of GLib, via GObject-Introspection and PyGI (uses the GLib main loop and object model)
- QtDBus, part of Qt, via PyQt (uses the Qt main loop and object model)

[dbus-python tutorial](https://dbus.freedesktop.org/doc/dbus-python/tutorial.html#id4)

